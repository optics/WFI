# ******************************************************************************
#
#   Copyright (C) 2021 Paul Scherrer Institut Villigen, Switzerland
#   
#   Author Xiaoqiang Wang,
#          Juraj Krempasky, juraj.krempasky@psi.ch
#          Uwe Flechsig,    uwe.flechsig@psi.ch
#
# ------------------------------------------------------------------------------
#
#   This file is part of WFI.
#
#   WFI is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License, or
#   (at your option) any later version.
#
#   WFI is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with WFI (LICENSE).  If not, see <http://www.gnu.org/licenses/>. 
#
# ******************************************************************************

include /ioc/tools/driver.makefile
EXCLUDE_VERSIONS = 3.13 3.14.8
BUILDCLASSES = Linux
ARCH_FILTER = ${EPICS_HOST_ARCH}
PROJECT = XGI

USR_CXXFLAGS += -DUSE_DOUBLE_PRECISION -I ../fftw
USR_CXXFLAGS += -DHAVE_HDF5
USR_LDFLAGS += -L ../fftw
USR_LIBS += fftw3

SOURCES += src/xgi.cpp
SOURCES += src/NDPluginXGI.cpp
SOURCES += src/algorithms.cpp
SOURCES += src/unwrap_phase.cpp

DBDS += src/NDPluginXGI.dbd

TEMPLATES += Db/NDXGI.template
TEMPLATES += Db/NDXGI_settings.req

SCRIPTS += startup.cmd

ADL += op/adl/NDPluginXGI.adl
ADL += op/adl/NDPluginXGI_extension.adl

# UF
TAGS:   
	etags src/*.cpp src/*.h 

distclean: 
	rm -f TAGS src/TAGS test/core.* *~ .*~ src/*~ src/.*~ test/*~ test/.*~ 


