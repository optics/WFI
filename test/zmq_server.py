from __future__ import print_function

import time
import zmq
import h5py
import numpy
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--type', dest='stype', type=str, default='PUB')
parser.add_argument('--host', dest='host', type=str, default='tcp://*:5432')
parser.add_argument('--rate', dest='rate', type=int, default=1)
parser.add_argument('fname', type=str, nargs=1, help='HDF5 file name')

args = parser.parse_args()
if args.stype == 'PUB':
    stype = zmq.PUB
elif args.stype == 'PUSH':
    stype = zmq.PUSH
else:
    stype = zmq.PUB
    print("Unsupported socket type %s, use PUB"%args.stype)

print("Server %s %s"%(args.host, args.stype))

context = zmq.Context()
sock = context.socket(stype)
sock.setsockopt(zmq.SNDHWM, 1)

if stype == zmq.PUB:
    sock.bind(args.host)
elif stype == zmq.PUSH:
    sock.connect(args.host)

cols = 800
rows = 600

header_t = """{
    "htype" : ["chunk-1.0"],
    "shape" : [%d,%d],
    "type"  : "%s",
    "frame" : %d,
    "ndattr": {
        "NumImages" : 1
    }
}"""

frame = 0
while True:
    time.sleep(1./args.rate)
    # read data from hdf5 file
    data = numpy.array(h5py.File(args.fname[0])['/entry/instrument/detector/data']).astype(numpy.uint16)
    rows, cols = data.shape
    # send header
    header = header_t % (cols, rows, data.dtype, frame)
    aheader= header.encode('ascii')  # UF add to be compatible with python3
    # sock.send(header, zmq.SNDMORE) # UF so wars
    sock.send(aheader, zmq.SNDMORE)  # UF new
    # send data
    sock.send(data)

    frame += 1
