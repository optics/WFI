#!/bin/bash                                         

if [ $# -ne 1 ]; then
    echo "usage  :   start_ioc.sh IOCname"
    echo "example: ./start_ioc.sh 13XGI"
    exit
fi

# define some variables
IOC=$1
TEST_CHANNEL="${IOC}1:Algorithm"                                 
CMD="iocsh"                
TEMPLATE="zmq_test_ioc_startup.script"
STARTUP_SCRIPT="${IOC}_${TEMPLATE}.tmp"
SERVICE="${CMD} ${STARTUP_SCRIPT}"

# prepare startup script and substitution file
echo "create dynamic startup script for IOC: ${IOC}"
# cd $RUNPATH
sed "s/13XGI/$IOC/g" ${TEMPLATE} > ${STARTUP_SCRIPT}

if ps ax | grep -v grep | grep "${SERVICE}" > /dev/null
then
    echo "!!! $SERVICE is already running !!!"
    echo "exit"
else
   if caget $TEST_CHANNEL &> /dev/null
   then
      echo "!!! channel $TEST_CHANNEL is already available !!!"
   else
      #cd $RUNPATH
      $CMD ${STARTUP_SCRIPT}
      exit
   fi
fi

