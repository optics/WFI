#!/bin/bash                                         

if [ $# -ne 1 ]; then
    echo "usage  :   set_defaults.sh IOCname"
    echo "example: ./set_defaults.sh 13XGI"
    exit
fi

# define some variables
IOC=$1
PREFIX=${IOC}1

caput ${PREFIX}:cam1:ArrayCallbacks "Enable"
caput ${PREFIX}:ROI1:MinX 505
caput ${PREFIX}:ROI1:SizeX 322
caput ${PREFIX}:ROI1:MinY 499
caput ${PREFIX}:ROI1:SizeY 335
caput ${PREFIX}:ROI1:EnableCallbacks "Enable"
caput ${PREFIX}:Proc1:NDArrayPort "ROI11"
caput ${PREFIX}:Proc1:DataTypeOut "Float64"
caput ${PREFIX}:Proc1:EnableCallbacks "Enable"
caput ${PREFIX}:XGI1:NDArrayPort "PROC1"
caput ${PREFIX}:XGI1:GratingAngle1 0.0271
caput ${PREFIX}:XGI1:GratingAngle2 -0.0185
caput ${PREFIX}:XGI1:GratingPeriod1 3.75e-06
caput ${PREFIX}:XGI1:GratingPeriod2 2e-06
caput ${PREFIX}:XGI1:GratingDistance 0.144
caput ${PREFIX}:XGI1:TalbotOrder 11
caput ${PREFIX}:XGI1:Energy 9000
caput ${PREFIX}:XGI1:PixelSize 2.857e-06
caput ${PREFIX}:XGI1:FFTPadding 16
caput ${PREFIX}:XGI1:FFTExclude 13
caput ${PREFIX}:XGI1:FFTFilter 3
caput ${PREFIX}:XGI1:FFTCrop 50
caput ${PREFIX}:XGI1:FFTNumThread 1
caput ${PREFIX}:XGI1:EnableCallbacks "Enable"
# end