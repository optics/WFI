# NDStdArraysConfigure (
#               portName,          # The name of the asyn port to be created
#               queueSize,         # The number of NDArrays that the input queue for this plugin can hold when 
#                                     NDPluginDriverBlockingCallbacks=0. 
#               blockingCallbacks, # 0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
#                                      of the driver doing the callbacks.
#               NDArrayPort,       # Port name of NDArray source
#               NDArrayAddr,       # Address of NDArray source
# )
NDStdArraysConfigure("Image$(N=1)", 20, 0, "$(NDARRAY_PORT)", "$(NDARRAY_ADDR)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image$(N=1):,PORT=Image$(N=1),ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(NDARRAY_PORT),NDARRAY_ADDR=$(NDARRAY_ADDR),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")

set_pass0_restoreFile("NDStdArrays_settings$(N=1).sav")
set_pass1_restoreFile("NDStdArrays_settings$(N=1).sav")

afterInit create_monitor_set,"NDStdArrays_settings.req",30,"P=$(PREFIX),R=image$(N=1):","NDStdArrays_settings$(N=1).sav"
