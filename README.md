PSI Wavefront Interferometry
============================

This is an areaDetector plugin for X-ray Grating Interferometry analysis.

NDPluginXGI defines the following parameters in NDPluginXGI.cpp and NDXGI.template.

| Parameter index variable | asyn interface | Access | Description | drvInfo string | EPICS record name | EPICS record type |
| ------------------------ | -------------- | ------ | ----------- | -------------- | ----------------- | ----------------- |
| XGIPluginAlgorithm       | asynInt32      | r/w    | Algorithm choices.<BR>  0 = "Standard" | XGI\_ALGORITHM | $(P)$(R)Algorithm<BR>$(P)$(R)Algorithm\_RBV | ao<BR>ai |
| XGIPluginGratingAngle1   | asynFloat64    | r/w    | 1st grating angle | XGI\_GRAT\_ANGLE\_1 | $(P)$(R)GratingAngle1<BR>$(P)$(R)GratingAngle1\_RBV | ao<BR>ai |
| XGIPluginGratingAngle2   | asynFloat64    | r/w    | 2nd grating angle | XGI\_GRAT\_ANGLE\_2 | $(P)$(R)GratingAngle2<BR>$(P)$(R)GratingAngle2\_RBV | ao<BR>ai |
| XGIPluginGratingPeriod1  | asynFloat64    | r/w    | 1st grating period | XGI\_GRAT\_PERIOD\_1 | $(P)$(R)GratingPeriod1<BR>$(P)$(R)GratingPeriod1\_RBV | ao<BR>ai |
| XGIPluginGratingPeriod2  | asynFloat64    | r/w    | 2nd grating period | XGI\_GRAT\_PERIOD\_2 | $(P)$(R)GratingPeriod2<BR>$(P)$(R)GratingPeriod2\_RBV | ao<BR>ai |
| XGIPluginGratingDistance | asynFloat64    | r/w    | Distance between 1st and 2nd grating | XGI\_GRAT\_DISTANCE | $(P)$(R)GratingDistance<BR>$(P)$(R)GratingDistance\_RBV | ao<BR>ai |
| XGIPluginTalbotiOrder    | asynInt32      | r/w    | Talbot order | XGI\_TALBOT\_ORDER | $(P)$(R)TalbotOrder<BR>$(P)$(R)TalbotOrder\_RBV | longout<BR>longin |
| XGIPluginEnergy          | asynFloat64    | r/w    | Photon energy | XGI\_ENERGY | $(P)$(R)Energy<BR>$(P)$(R)Energy\_RBV | ao<BR>ai |
| XGIPluginPixelSize       | asynFloat64    | r/w    | Physical pixel size | XGI\_PIXEL\_SIZE | $(P)$(R)PixelSize<BR>$(P)$(R)PixelSize\_RBV | ao<BR>ai |
| XGIPluginFFTPadding      | asynInt32      | r/w    | FFT zero padding | XGI\_FFT\_PADDING | $(P)$(R)FFTPadding<BR>$(P)$(R)FFTPadding\_RBV | longout<BR>longin |
| XGIPluginFFTExclude      | asynInt32      | r/w    | FFT exclude param | XGI\_FFT\_EXCLUDE | $(P)$(R)FFTExclude<BR>$(P)$(R)FFTExclude\_RBV | longout<BR>longin |
| XGIPluginFFTFilter       | asynInt32      | r/w    | FFT filter param | XGI\_FFT\_FILTER | $(P)$(R)FFTFilter<BR>$(P)$(R)FFTFilter\_RBV | longout<BR>longin |
| XGIPluginFFTCrop         | asynInt32      | r/w    | FFT crop size | XGI\_FFT\_CROP | $(P)$(R)FFTCrop<BR>$(P)$(R)FFTCrop\_RBV | longout<BR>longin |
| XGIPluginFFTNumThread    | asynInt32      | r/w    | FFT number of threads | XGI\_FFT\_NUM\_THREAD | $(P)$(R)FFTNumThread<BR>$(P)$(R)FFTNumThread\_RBV | longout<BR>longin |
| XGIPluginBackgroundFile  | asynOctet      | r/w    | Background file in hdf5 format. The image data is assumed to be of uint16 type, having the same dimension as the input NDArray, at path "/entry/instrument/detector/data" | XGI\_BACKGROUND\_FILE | $(P)$(R)BackgroundFile<BR>$(P)$(R)BackgroundFile\_RBV | waveform |
| XGIPluginProfileAxis     | asynFloat64Array | r/o  | Height profile axis | XGI\_PROFILE\_AXIS | $(P)$(R)ProfileAxis\_RBV | waveform |
| XGIPluginProfileData     | asynFloat64Array | r/o  | Height profile data | XGI\_PROFILE\_DATA | $(P)$(R)ProfileData\_RBV | waveform |
| XGIPluginROC             | asynFloat64     | r/o   | Radius of curvature | XGI\_ROC | $(P)$(R)RadiusOfCurvature\_RBV | ai |

It also exports the intermediate processing resutls as NDArrays,

| asyn addr | ndims | Description |
|:---------:|:-----:| ----------- |
| 0         | 1     | Profile data. Same as $(P)$(R)ProfileData. |
| 1         | 2     | FFT amplitude image. |
| 2         | 2     | FFT phase wrapped. |
| 3         | 2     | FFT phase unwrapped. |

Build and Install
-----------------
It uses PSI driver.makefile to build and install.

```
$ make build
$ make install
```
install at pc13013 (after build at gfalc)
```
$ ssh saresa-gw
$ ssh saresa-cons-04
$ ssh pc13023
$ cd git/WFI
$ make -f Makefile.pc13023 install
```

Test
----
An IOC under *test* folder can be used for tests. It uses ZMQDriver to acquire test images.

Create the folder for autoSR
```
$ mkdir /home/ioc/archiver-data/autoSR/13XGI
```

Start the IOC
a)
```
$ cd test
$ iocsh zmq_test_ioc_startup.script
```
b)
```
$ test/start_ioc.sh 13XGI
```

c) (force EPICS 7)
```
$ iocsh -7.0.5 zmq_test_ioc_startup.script
```

Start the MEDM
a)
```
$ ./runui 13XGI1:
```
b)
```
$ test/runui-local 13XGI
```

Setup parameters
a)
```
$ caput 13XGI1:cam1:ArrayCallbacks "Enable"
$ caput 13XGI1:ROI1:MinX 505
$ caput 13XGI1:ROI1:SizeX 322
$ caput 13XGI1:ROI1:MinY 499
$ caput 13XGI1:ROI1:SizeY 335
$ caput 13XGI1:ROI1:EnableCallbacks "Enable"
$ caput 13XGI1:Proc1:NDArrayPort "ROI11"
$ caput 13XGI1:Proc1:DataTypeOut "Float64"
$ caput 13XGI1:Proc1:EnableCallbacks "Enable"
$ caput 13XGI1:XGI1:NDArrayPort "PROC1"
$ caput 13XGI1:XGI1:GratingAngle1 0.0271
$ caput 13XGI1:XGI1:GratingAngle2 -0.0185
$ caput 13XGI1:XGI1:GratingPeriod1 3.75e-06
$ caput 13XGI1:XGI1:GratingPeriod2 2e-06
$ caput 13XGI1:XGI1:GratingDistance 0.144
$ caput 13XGI1:XGI1:TalbotOrder 11
$ caput 13XGI1:XGI1:Energy 9000
$ caput 13XGI1:XGI1:PixelSize 2.857e-06
$ caput 13XGI1:XGI1:FFTPadding 16
$ caput 13XGI1:XGI1:FFTExclude 13
$ caput 13XGI1:XGI1:FFTFilter 3
$ caput 13XGI1:XGI1:FFTCrop 50
$ caput 13XGI1:XGI1:FFTNumThread 1
$ caput 13XGI1:XGI1:EnableCallbacks "Enable"
```
b)
```
test/set_defaults.sh 13XGI
```

Start ZMQDriver acquisition
```
$ caput 13XGI1:cam1:Acquire 1
```

Start the ZMQ server to publish the test image
```
$ source /opt/gfa/python
$ python zmq_server.py sfb_0087.h5
$ python zmq_server.py 190315_jkuf.h5
```

Start the viewer, use Array address 2
```
python /afs/psi.ch/project/soft_x/OpticsTools/python/ADLiveView.py 13XGI1:image1:
python /afs/psi.ch/project/soft_x/OpticsTools/python/ADLiveView.py 13XGI1:image2:
```