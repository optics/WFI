# -*-sh-*- emacs hint
# ******************************************************************************
#
#   Copyright (C) 2021 Paul Scherrer Institut Villigen, Switzerland
#   
#   Author Xiaoqiang Wang,
#          Juraj Krempasky, juraj.krempasky@psi.ch
#          Uwe Flechsig,    uwe.flechsig@psi.ch
#
# ------------------------------------------------------------------------------
#
#   This file is part of WFI.
#
#   WFI is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License, or
#   (at your option) any later version.
#
#   WFI is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with WFI (LICENSE).  If not, see <http://www.gnu.org/licenses/>. 
#
# ******************************************************************************

set_requestfile_path("$($(MODULE)_DIR)db")

# NDXGIConfigure (
#       portName,           The name of the asyn port driver to be created.
#       queueSize,          The number of NDArrays that the input queue for this plugin can hold when
#                           NDPluginDriverBlockingCallbacks=0.
#       blockingCallbacks,  0=callbacks are queued and executed by the callback thread;
#                           1 callbacks execute in the thread of the driver doing the callbacks.
#       NDArrayPort,        Port name of NDArray source
#       NDArrayAddr)        Address of NDArray source
NDXGIConfigure("XGI$(N=1)", 20, 0, "$(PORT)", 0)
dbLoadRecords("NDXGI.template","P=$(PREFIX),R=XGI$(N=1):,PORT=XGI$(N=1),ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NCHANS=$(NCHANS=1024)")

set_pass0_restoreFile("NDXGI_settings$(N=1).sav")
set_pass1_restoreFile("NDXGI_settings$(N=1).sav")

afterInit create_monitor_set,"NDXGI_settings.req",30,"P=$(PREFIX),R=XGI$(N=1):","NDXGI_settings$(N=1).sav"
