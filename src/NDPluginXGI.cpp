// Time-stamp: <2021-11-03 11:04:03 flechsig>  
/*
 * NDPluginXGI.cpp
 * 
 * areaDetecotor plugin for X-ray Grating Interferometry (XGI) analysis.
 *
 * Author: Xiaoqiang Wang
 *
 * Created Jan. 29, 2019
 * Extensions UF Feb 11, 2019 
 */
// ******************************************************************************
//
//   Copyright (C) 2021 Paul Scherrer Institut Villigen, Switzerland
//   
//   Author Xiaoqiang Wang,
//          Juraj Krempasky, juraj.krempasky@psi.ch
//          Uwe Flechsig,    uwe.flechsig@psi.ch
//
// ------------------------------------------------------------------------------
//
//   This file is part of WFI.
//
//   WFI is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, version 3 of the License, or
//   (at your option) any later version.
//
//   WFI is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with WFI (LICENSE).  If not, see <http://www.gnu.org/licenses/>. 
//
// ******************************************************************************


#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <iocsh.h>
#include <sstream>

#include "NDPluginDriver.h"
#include <ADCoreVersion.h>

#include "xgi.h"
#include "algorithms.h"

#define XGI_MAX_OUTPUT 4 /* Number of NDArray outputs */

static const char *driverName="NDPluginXGI";

#include <epicsExport.h>

#define XGIPluginAlgorithmString       "XGI_ALGORITHM"      /* (asynInt32,   r/w) Algorithm choices */
#define XGIPluginGratingAngle1String   "XGI_GRAT_ANGLE_1"   /* (asynFloat64, r/w) 1st grating beta angle */
#define XGIPluginGratingAngle2String   "XGI_GRAT_ANGLE_2"   /* (asynFloat64, r/w) 2nd grating beta angle */
#define XGIPluginGratingPeriod1String  "XGI_GRAT_PERIOD_1"  /* (asynFloat64, r/w) 1st grating period */
#define XGIPluginGratingPeriod2String  "XGI_GRAT_PERIOD_2"  /* (asynFloat64, r/w) 2nd grating period */
#define XGIPluginGratingDistanceString "XGI_GRAT_DISTANCE"  /* (asynFloat64, r/w) Distance between 1st and 2nd grating */
#define XGIPluginTalbotOrderString     "XGI_TALBOT_ORDER"   /* (asynInt32,   r/w) Talbot order */
#define XGIPluginEnergyString          "XGI_ENERGY"         /* (asynFloat64, r/w) Photon energy */
#define XGIPluginPixelSizeString       "XGI_PIXEL_SIZE"     /* (asynFloat64, r/w) Physical pixel size */
#define XGIPluginFFTPaddingString      "XGI_FFT_PADDING"    /* (asynInt32,   r/w) FFT padding */
#define XGIPluginFFTExcludeString      "XGI_FFT_EXCLUDE"    /* (asynInt32,   r/w) FFT exclude param */
#define XGIPluginFFTFilterString       "XGI_FFT_FILTER"     /* (asynInt32,   r/w) FFT filter */
#define XGIPluginFFTCropString         "XGI_FFT_CROP"       /* (asynInt32,   r/w) FFT crop */
#define XGIPluginFFTNumThreadString    "XGI_FFT_NUM_THREAD" /* (asynInt32,   r/w) FFT numer of threads */
#define XGIPluginBackgroundFileString  "XGI_BACKGROUND_FILE"/* (asynOctet,   r/w) Background file */
#define XGIPluginProfileAxisString     "XGI_PROFILE_AXIS"   /* (asynFloat64Array, r/o) Profile axis */
#define XGIPluginProfileDataString     "XGI_PROFILE_DATA"   /* (asynFloat64Array, r/o) Profile data */
#define XGIPluginROCString             "XGI_ROC"            /* (asynFloat64, r/o) Radius of curvature */
// UF add
#define XGIPluginMinorString          "XGI_MINOR"      /* (asynInt32,        r/w) Minor calc mode   */
#define XGIPluginCenterString         "XGI_CENTER"     /* (asynInt32,        r/w) FFT filter center */
#define XGIPluginWidthString          "XGI_WIDTH"      /* (asynInt32,        r/w) FFT filter width  */
#define XGIPluginLogscaleString       "XGI_LOGSCALE"   /* (asynInt32,        r/w) logscale          */
#define XGIPluginOutputsString        "XGI_OUTPUTS"    /* (asynInt32,        r/w) phase output      */
#define XGIPluginAveragesString       "XGI_AVERAGES"   /* (asynInt32,        r/w) averages          */
#define XGIPluginAverCountString      "XGI_AVERCOUNT"  /* (asynInt32,        r/w) averages counter  */

/** Base class for XGI analysis plugin. */
class NDPluginXGI : public NDPluginDriver {
public:
    NDPluginXGI(const char *portName, int queueSize, int blockingCallbacks, 
		const char *NDArrayPort, int NDArrayAddr,
		int maxBuffers, size_t maxMemory,
		int priority, int stackSize);

    ~NDPluginXGI();

    /* These methods override those in the base class */
    virtual void processCallbacks(NDArray *pArray);
    virtual asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual);

protected:
    /* These methods are new in this class */
    NDArray *matrixToNDArray(const Matrix *matrix);

private:
    Xgi xgi;
    Xgi_ip input;
    Xgi_out output;
    Matrix background;

    int XGIPluginAlgorithm;
    #define FIRST_NDPLUGIN_XGI_PARAM  XGIPluginAlgorithm
    int XGIPluginGratingAngle1;
    int XGIPluginGratingAngle2;
    int XGIPluginGratingPeriod1;
    int XGIPluginGratingPeriod2;
    int XGIPluginGratingDistance;
    int XGIPluginTalbotOrder;
    int XGIPluginEnergy;
    int XGIPluginPixelSize;
    int XGIPluginFFTPadding;
    int XGIPluginFFTExclude;
    int XGIPluginFFTFilter;
    int XGIPluginFFTCrop;
    int XGIPluginFFTNumThread;
    int XGIPluginBackgroundFile;
    int XGIPluginProfileAxis;
    int XGIPluginProfileData;
  // UF begin
    int XGIPluginMinor;
    int XGIPluginCenter;
    int XGIPluginWidth;
    int XGIPluginLogscale;
    int XGIPluginOutputs;
    int XGIPluginAverages;
    int XGIPluginAverCount;
  // UF end
    int XGIPluginROC;
    #define LAST_NDPLUGIN_XGI_PARAM XGIPluginROC
    
    struct ConfigStruct XGIconfig;  // UF config structure defined in algorithms.h
};
#define NUM_NDPLUGIN_XGI_PARAMS ((int)(&LAST_NDPLUGIN_XGI_PARAM - &FIRST_NDPLUGIN_XGI_PARAM + 1))

/** Callback function that is called by the NDArray driver with new NDArray data.
  * \param[in] pArray  The NDArray from the callback.
  */ 
void NDPluginXGI::processCallbacks(NDArray *pArray)
{
    int arrayCounter, arrayCallbacks;
    NDArrayInfo_t arrayInfo;
    
    const char* functionName = "processCallbacks";

    /* Most plugins want to increment the arrayCounter each time they are called, which NDPluginDriver
     * does.  However, for this plugin we only want to increment it when we actually got a callback we were
     * supposed to save.  So we save the array counter before calling base method, increment it here */
    getIntegerParam(NDArrayCounter, &arrayCounter);
    getIntegerParam(NDArrayCallbacks, &arrayCallbacks);

    /* Call the base class method */
    NDPluginDriver::processCallbacks(pArray);

    /* only 2d image of double data type is accepted */
    if (pArray->dataType != NDFloat64 || pArray->ndims != 2) {
        asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
                "%s:%s: Only 2d images of double data type are accepted - return\n",
                driverName, functionName);
        return;
    }
 
    /* get NDArray info */
    pArray->getInfo(&arrayInfo);

    /* Get user input */ 
    int algorithm, numThread, padding, exclude, filter, crop, talbotOrder;
    double gratingAngle1, gratingAngle2, gratingPeriod1, gratingPeriod2, distance, energy, pixelSize;

    getIntegerParam(XGIPluginAlgorithm, &algorithm);
    getDoubleParam(XGIPluginGratingAngle1, &gratingAngle1);
    getDoubleParam(XGIPluginGratingAngle2, &gratingAngle2);
    getDoubleParam(XGIPluginGratingPeriod1, &gratingPeriod1);
    getDoubleParam(XGIPluginGratingPeriod2, &gratingPeriod2);
    getDoubleParam(XGIPluginGratingDistance, &distance);
    getIntegerParam(XGIPluginTalbotOrder, &talbotOrder);
    getDoubleParam(XGIPluginEnergy, &energy);
    getDoubleParam(XGIPluginPixelSize, &pixelSize);

    getIntegerParam(XGIPluginFFTNumThread, &numThread);
    getIntegerParam(XGIPluginFFTPadding, &padding);
    getIntegerParam(XGIPluginFFTExclude, &exclude);
    getIntegerParam(XGIPluginFFTFilter, &filter);
    getIntegerParam(XGIPluginFFTCrop, &crop);
    // UF add
    getIntegerParam(XGIPluginMinor,     &this->XGIconfig.minor);
    getIntegerParam(XGIPluginCenter,    &this->XGIconfig.center);
    getIntegerParam(XGIPluginWidth,     &this->XGIconfig.width);
    getIntegerParam(XGIPluginLogscale,  &this->XGIconfig.logscale);
    getIntegerParam(XGIPluginOutputs,   &this->XGIconfig.outputs);
    getIntegerParam(XGIPluginAverages,  &this->XGIconfig.averages);

    /* Release the lock; this is computationally intensive and does not access any shared data */
    this->unlock();

    /* Fill in XGI inputs */
    input.beta_fix = gratingAngle1;
    input.beta_rot = gratingAngle2;
    input.period_g1 = gratingPeriod1;
    input.period_g2 = gratingPeriod2;
    input.distance = distance;
    input.talbot_order = talbotOrder;
    input.energy = energy;
    input.pixel_size = pixelSize;
    input.n_zeropadding = padding;
    input.excl_para = exclude;
    input.filter_para = filter;
    input.ncrop = crop;
    input.fftw_thread = numThread;
    input.roi.x_lo = 0;
    input.roi.x_hi = arrayInfo.xSize;
    input.roi.y_lo = 0;
    input.roi.y_hi = arrayInfo.ySize;

    /* Performance Measurement : tic */ 
    epicsTimeStamp tic, toc;
    epicsTimeGetCurrent(&tic);

    /* Copy input NDArray */
    Matrix image;
    if (image.resize(arrayInfo.xSize, arrayInfo.ySize) != 0) {
        this->lock();
        return;
    }
    memcpy(image.data, pArray->pData, arrayInfo.totalBytes);
    /* Subtract background if any */
    if (background.data)
        image.sub(&background);

    /* Compute */
    switch(algorithm) {
    case 0:
        xgi.compute(&image, &input, &output);
        break;
    case 1:
	printf("Algorithm == %d not yet implemented\n", algorithm);
	// waveform_advanced();
	break;
    case 2:
      printf("Algorithm == %d not yet implemented\n", algorithm);
      break;
    case 3:
      #ifdef DEBUG
         printf("Algorithm == %d --> call evalfft2d()\n", algorithm);
      #endif
	 evalfft2d(&image, &this->XGIconfig, &output);
	 break;
    case 4:
        #ifdef DEBUG
           printf("Algorithm == %d --> call evaluf()\n", algorithm);
        #endif
	   this->XGIconfig.direction= COLDIR;
	   evaluf(&image, &this->XGIconfig, &output);
	   break;
    case 5:
        #ifdef DEBUG
	   printf("Algorithm == %d --> call evaluf()\n", algorithm);
        #endif
	   this->XGIconfig.direction= ROWDIR;
	   evaluf(&image, &this->XGIconfig, &output);
	   break;
    case 6:
      #ifdef DEBUG
           printf("Algorithm == %d --> call evalfft_ifft_2d()\n", algorithm);
      #endif
           evalfft_ifft_2d(&image, &this->XGIconfig, &output);
            break;
    case 7:
        #ifdef DEBUG
	   printf("Algorithm == %d --> call evaluf()\n", algorithm);
        #endif
	   this->XGIconfig.direction= COLDIR;
	   evaluf_zp(&image, &this->XGIconfig, &output);
	   break;
    case 8:
        #ifdef DEBUG
	   printf("Algorithm == %d --> call evaluf()\n", algorithm);
        #endif
	   this->XGIconfig.direction= ROWDIR;
	   evaluf_zp(&image, &this->XGIconfig, &output);
	   break;
    case 9:
      #ifdef DEBUG
        printf("Algorithm == %d --> just overlay plus callback \n", algorithm);
      #endif
	myoverlay(&image, &this->XGIconfig, &output);
      break;
    default:
        asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
                 "%s:%s: Algorithm %d not implemented\n",
                 driverName, functionName, algorithm);
    } // end algorithm

    /* Free copy */
    image.free();

    /* Performance Measurement : toc */ 
    epicsTimeGetCurrent(&toc);
    asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER,
            "XGI consumes %f seconds\n",
            epicsTimeDiffInSeconds(&toc, &tic));

    /* Create NDArray callbacks for results matrix */
    if (arrayCallbacks) {
        for (int i = 0; i < XGI_MAX_OUTPUT; i++) {
            Matrix *m = NULL;
            switch (i)   // set pointer
	      {
	        case 0: m = &(output.hp);          break; // Profile 
	        case 1: m = &(output.amp);         break; // Amplitude 
	        case 2: m = &(output.dphi_wrap);   break; // Wrapped Phase 
	        case 3: m = &(output.dphi_unwrap); break; // Unwrapped Phase 
	        default: continue;
	      }

            if (this->pArrays[i]) this->pArrays[i]->release();
            this->pArrays[i] = matrixToNDArray(m); // fills data from pointer into field
            if (this->pArrays[i] == NULL) continue;

            /* Copy time stamp and uniqueId from input array */
	    this->pArrays[i]->uniqueId = pArray->uniqueId;
	    this->pArrays[i]->timeStamp = pArray->timeStamp;
	    /* Update attribute list */
	    this->getAttributes(this->pArrays[i]->pAttributeList);
	    /* Call the NDArray callback */
	    doCallbacksGenericPointer(this->pArrays[i], NDArrayData, i);
        }
    }
    /* Leave output matrix, they are reused for the next processing */

    /* Update the parameters.  */
    this->lock();
    arrayCounter++;
    setIntegerParam(NDArrayCounter, arrayCounter);
    //setIntegerParam(XGIPluginAverCount, arrayCounter);  // uf
    setDoubleParam(XGIPluginROC, output.roc);
    doCallbacksFloat64Array(output.laxis.data, output.laxis.width*output.laxis.height, XGIPluginProfileAxis, 0);
    doCallbacksFloat64Array(output.hp.data, output.hp.width*output.hp.height, XGIPluginProfileData, 0);
    callParamCallbacks();
} // end processCallbacks


/** Called when asyn clients call pasynOctet->write().
  * This function performs actions for some parameters, including NDPluginDriverArrayPort.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Address of the string to write.
  * \param[in] nChars Number of characters to write.
  * \param[out] nActual Number of characters actually written. */
asynStatus NDPluginXGI::writeOctet(asynUser *pasynUser, const char *value,
                                    size_t nChars, size_t *nActual)
{
    int addr=0;
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    static const char *functionName = "writeOctet";

    status = getAddress(pasynUser, &addr); if (status != asynSuccess) return(status);
    /* Set the parameter in the parameter library. */
    status = (asynStatus)setStringParam(addr, function, (char *)value);

    if (function == XGIPluginBackgroundFile) {
        // delete previouse background image if any
        if (background.data) {
            background.free();
        }
        // load from background file
        if (strlen(value) > 0) {
            background.load_h5_ushort(value, "/entry/instrument/detector/data", 0);
        }
        if (background.data == NULL) {
            setStringParam(addr, function, (char*)"");
            status = asynError;
        }
    } else {
        /* If this parameter belongs to a base class call its method */
        if (function < FIRST_NDPLUGIN_XGI_PARAM)
            status = NDPluginDriver::writeOctet(pasynUser, value, nChars, nActual);
    }

     /* Do callbacks so higher layers see any changes */
    status = (asynStatus)callParamCallbacks(addr);

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%d, function=%d, value=%s",
                  driverName, functionName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, value=%s\n",
              driverName, functionName, function, value);
    *nActual = nChars;
    return status;
}

NDArray *NDPluginXGI::matrixToNDArray(const Matrix *matrix)
{
    int ndims = 2;
    
    if (matrix->data == NULL) // UF it fails in case the matrix is not allocated
    {
#ifdef DEBUG
        std::cerr << "info: matrixToNDArray: matrix == NULL" << std::endl;
#endif
	return NULL;
    }

    size_t dims[2] = {matrix->width, matrix->height};
    NDDataType_t dataType = NDFloat64;
    size_t dataSize = matrix->width * matrix->height * sizeof(double);
    static const char *functionName = "matrixToNDArray";

    NDArray *pArrayOut = this->pNDArrayPool->alloc(ndims, dims, dataType, dataSize, NULL);
    if (pArrayOut != NULL) 
      memcpy(pArrayOut->pData, matrix->data, dataSize);
    else 
      asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
                "%s:%s alloc NDArray error\n",
                driverName, functionName);
    
    return pArrayOut;
}  // end NDPluginXGI::matrixToNDArray

/** Constructor for NDPluginXGI; all parameters are simply passed to NDPluginDriver::NDPluginDriver.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] queueSize The number of NDArrays that the input queue for this plugin can hold when 
  *            NDPluginDriverBlockingCallbacks=0.  Larger queues can decrease the number of dropped arrays,
  *            at the expense of more NDArray buffers being allocated from the underlying driver's NDArrayPool.
  * \param[in] blockingCallbacks Initial setting for the NDPluginDriverBlockingCallbacks flag.
  *            0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
  *            of the driver doing the callbacks.
  * \param[in] NDArrayPort Name of asyn port driver for initial source of NDArray callbacks.
  * \param[in] NDArrayAddr asyn port driver address for initial source of NDArray callbacks.
  * \param[in] maxAddr The maximum  number of asyn addr addresses this driver supports. 1 is minimum.
  * \param[in] numParams The number of parameters supported by the derived class calling this constructor.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is 
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is 
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] interfaceMask Bit mask defining the asyn interfaces that this driver supports.
  * \param[in] interruptMask Bit mask definining the asyn interfaces that can generate interrupts (callbacks)
  * \param[in] asynFlags Flags when creating the asyn port driver; includes ASYN_CANBLOCK and ASYN_MULTIDEVICE.
  * \param[in] autoConnect The autoConnect flag for the asyn port driver.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
NDPluginXGI::NDPluginXGI(const char *portName, int queueSize, int blockingCallbacks, 
                           const char *NDArrayPort, int NDArrayAddr,
                           int maxBuffers, size_t maxMemory,
                           int priority, int stackSize)
    /* Invoke the base class constructor.
     * We allocate 1 NDArray of unlimited size in the NDArray pool.
     * This driver can block (because writing a file can be slow), and it is not multi-device.  
     * Set autoconnect to 1.  priority and stacksize can be 0, which will use defaults. */
    : NDPluginDriver(portName, queueSize, blockingCallbacks, 
                     NDArrayPort, NDArrayAddr, XGI_MAX_OUTPUT,
#if ADCORE_VERSION < 3
                     NUM_NDPLUGIN_XGI_PARAMS,
#endif
                     maxBuffers, maxMemory, 
                     asynFloat64ArrayMask | asynGenericPointerMask,
                     asynFloat64ArrayMask | asynGenericPointerMask,
                     ASYN_CANBLOCK | ASYN_MULTIDEVICE, 1, priority, stackSize
#if ADCORE_VERSION >= 3
                    , 1 /* single thread */, true /* compressionAware */
#endif
		     )
{
    /* Create params */
    createParam(XGIPluginAlgorithmString,      asynParamInt32,        &XGIPluginAlgorithm);
    createParam(XGIPluginGratingAngle1String,  asynParamFloat64,      &XGIPluginGratingAngle1);
    createParam(XGIPluginGratingAngle2String,  asynParamFloat64,      &XGIPluginGratingAngle2);
    createParam(XGIPluginGratingPeriod1String, asynParamFloat64,      &XGIPluginGratingPeriod1);
    createParam(XGIPluginGratingPeriod2String, asynParamFloat64,      &XGIPluginGratingPeriod2);
    createParam(XGIPluginGratingDistanceString,asynParamFloat64,      &XGIPluginGratingDistance);
    createParam(XGIPluginTalbotOrderString,    asynParamInt32,        &XGIPluginTalbotOrder);
    createParam(XGIPluginEnergyString,         asynParamFloat64,      &XGIPluginEnergy);
    createParam(XGIPluginPixelSizeString,      asynParamFloat64,      &XGIPluginPixelSize);
    createParam(XGIPluginFFTPaddingString,     asynParamInt32,        &XGIPluginFFTPadding);
    createParam(XGIPluginFFTExcludeString,     asynParamInt32,        &XGIPluginFFTExclude);
    createParam(XGIPluginFFTFilterString,      asynParamInt32,        &XGIPluginFFTFilter);
    createParam(XGIPluginFFTCropString,        asynParamInt32,        &XGIPluginFFTCrop);
    createParam(XGIPluginFFTNumThreadString,   asynParamInt32,        &XGIPluginFFTNumThread);
    createParam(XGIPluginBackgroundFileString, asynParamOctet,        &XGIPluginBackgroundFile);
    createParam(XGIPluginProfileAxisString,    asynParamFloat64Array, &XGIPluginProfileAxis);
    createParam(XGIPluginProfileDataString,    asynParamFloat64Array, &XGIPluginProfileData);
    createParam(XGIPluginROCString,            asynParamFloat64,      &XGIPluginROC);
// UF 
    createParam(XGIPluginMinorString,     asynParamInt32, &XGIPluginMinor);
    createParam(XGIPluginCenterString,    asynParamInt32, &XGIPluginCenter);
    createParam(XGIPluginWidthString,     asynParamInt32, &XGIPluginWidth);
    createParam(XGIPluginLogscaleString,  asynParamInt32, &XGIPluginLogscale);
    createParam(XGIPluginOutputsString,   asynParamInt32, &XGIPluginOutputs);
    createParam(XGIPluginAveragesString,  asynParamInt32, &XGIPluginAverages);
    createParam(XGIPluginAverCountString, asynParamInt32, &XGIPluginAverCount);

    /* Set the plugin type string */    
    setStringParam(NDPluginDriverPluginType, driverName);

    /* Try to connect to the NDArray port */
    connectToArrayPort();

    ReadConfig(&this->XGIconfig); // UF test to read a config file
}

NDPluginXGI::~NDPluginXGI()
{
}

/** Configuration command */
extern "C" int NDXGIConfigure(const char *portName, int queueSize, int blockingCallbacks,
                                 const char *NDArrayPort, int NDArrayAddr,
                                 int maxBuffers, size_t maxMemory,
                                 int priority, int stackSize)
{
    NDPluginXGI *pPlugin = new NDPluginXGI(portName, queueSize, blockingCallbacks, NDArrayPort, NDArrayAddr,
                      maxBuffers, maxMemory, priority, stackSize);
#if ADCORE_VERISON > 2 || (ADCORE_VERSION == 2 && ADCORE_REVISION >= 5)
    return pPlugin->start();
#else
    return asynSuccess;
#endif
}

/* EPICS iocsh shell commands */
static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "frame queue size",iocshArgInt};
static const iocshArg initArg2 = { "blocking callbacks",iocshArgInt};
static const iocshArg initArg3 = { "NDArrayPort",iocshArgString};
static const iocshArg initArg4 = { "NDArrayAddr",iocshArgInt};
static const iocshArg initArg5 = { "maxBuffers",iocshArgInt};
static const iocshArg initArg6 = { "maxMemory",iocshArgInt};
static const iocshArg initArg7 = { "priority",iocshArgInt};
static const iocshArg initArg8 = { "stackSize",iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6,
                                            &initArg7,
                                            &initArg8};
static const iocshFuncDef initFuncDef = {"NDXGIConfigure",9,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    NDXGIConfigure(args[0].sval, args[1].ival, args[2].ival,
                     args[3].sval, args[4].ival, args[5].ival,
                     args[6].ival, args[7].ival, args[8].ival);
}

extern "C" void NDXGIRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

extern "C" {
epicsExportRegistrar(NDXGIRegister);
}
