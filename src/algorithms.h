/* File      : /afs/psi.ch/user/f/flechsig/git/WFI/src/algorithms.h */
/* Date      : <11 Feb 19 10:03:58 flechsig>  */
/* Time-stamp: <2021-11-03 11:03:36 flechsig>  */
/* Author    : Flechsig Uwe, uwe.flechsig&#64;psi.&#99;&#104; */

// ******************************************************************************
//
//   Copyright (C) 2021 Paul Scherrer Institut Villigen, Switzerland
//   
//   Author Xiaoqiang Wang,
//          Juraj Krempasky, juraj.krempasky@psi.ch
//          Uwe Flechsig,    uwe.flechsig@psi.ch
//
// ------------------------------------------------------------------------------
//
//   This file is part of WFI.
//
//   WFI is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, version 3 of the License, or
//   (at your option) any later version.
//
//   WFI is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with WFI (LICENSE).  If not, see <http://www.gnu.org/licenses/>. 
//
// ******************************************************************************

#ifndef ALGORITMS_H
#define ALGORITMS_H 

#include "fftw3.h"

/* pi in double precision */
#define PI 3.141592653589793238462643383279502884197169399375105820974944592
#define COLDIR   0
#define ROWDIR   1

#ifndef CONFIGFILENAME
#define CONFIGFILENAME "/tmp/XGI_config.conf"
//  #define CONFIGFILENAME "/afs/psi.ch/user/f/flechsig/git/WFI/test/XGI_config.conf"
#endif

// configuration structure type
// to pass parameters by file
struct ConfigStruct {
  int averages;
  int center;
  int direction;
  int logscale;
  int minor;
  int outputs;
  int version;
  int width;
  int zeropadding;
  double mytestdouble;
  double R0; // design wavefront radius of curvature in m
  double d;  // distance between gratings in m
  double photon_energy; // in eV
  double eta;        // SR page 23 eta = 1 for pi/2 phase shifting grating or amplitude grating; eta=2 for pi .. grating
  int    talbot_n;   // fractional talbot order  
  double p1, p2;     // grating pitch in m  
  double pixel_size; // in m
};

// procedure prototypes
bool is_file_exist(const char *);
double linreg(int, double *, double *, double *, double *);
void autorange(double *, int, int, int);
void calc_alpha(double *, int, int, struct ConfigStruct *);
void eval1d(double *, int, int, struct ConfigStruct *, struct Xgi_out *);
void eval1d_zp(double *, int, int, struct ConfigStruct *, struct Xgi_out *);
void evalfft2d(Matrix *, struct ConfigStruct *, struct Xgi_out *);
void evalfft_ifft_2d(Matrix *, struct ConfigStruct *, struct Xgi_out *);
void evaluf(Matrix *, struct ConfigStruct *, struct Xgi_out *);
void evaluf_zp(Matrix *, struct ConfigStruct *, struct Xgi_out *);
void fftshift2d(fftw_complex *, int, int);
void fftshift1d(fftw_complex *, int);
void filter1d(fftw_complex *, int, int, int, int);
void hanning(double *, int, int, int);
void hanning1d(double *, int);
void myoverlay(Matrix *, struct ConfigStruct *, struct Xgi_out *);
void overlay(double *, int, int, int, int, double, int);
void ReadConfig(struct ConfigStruct *);
#endif
// end algorithms.h
