// File      : /afs/psi.ch/user/f/flechsig/git/WFI/src/algorithms.cpp
// Date      : <11 Feb 19 10:02:46 flechsig> 
// Time-stamp: <2021-11-03 11:02:52 flechsig> 
// Author    : Flechsig Uwe, uwe.flechsig&#64;psi.&#99;&#104;

// ******************************************************************************
//
//   Copyright (C) 2021 Paul Scherrer Institut Villigen, Switzerland
//   
//   Author Xiaoqiang Wang,
//          Juraj Krempasky, juraj.krempasky@psi.ch
//          Uwe Flechsig,    uwe.flechsig@psi.ch
//
// ------------------------------------------------------------------------------
//
//   This file is part of WFI.
//
//   WFI is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, version 3 of the License, or
//   (at your option) any later version.
//
//   WFI is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with WFI (LICENSE).  If not, see <http://www.gnu.org/licenses/>. 
//
// ******************************************************************************

#include <iostream>
#include <sstream>
#include <fstream>
#include <cfloat>
#include <cstring>
#include <cstdlib>

#include <stdlib.h>
#include <string.h>

#include "fftw3.h"
#include "xgi.h"
#include "algorithms.h"
#include "unwrap_phase.h"

using namespace std;

// scales the data to 0..0xff or 0xffff option is logscale 
void autorange(double *data, int rows, int cols, int logscale)
{
  int arrsize, i, maxval= 255;
  double min, max, val, scale;

#ifdef UINT16_OUT
  maxval= 0xFFFF;
#endif

#ifdef DEBUG
  cout << "autorange called, logscale= "<< logscale << ", maxval= " << maxval;
#endif

  arrsize= rows* cols;
  min= max= data[0];
  for (i= 0; i< arrsize; i++)
    {
      val= data[i];
      min= (val < min) ? val : min;
      max= (val > max) ? val : max;
    }
  if (logscale)
    {
      if (min < 0.0)
	{
	  cout << ", min < 0.0 -> no logscale possible -> return" << endl;
	  return;
	}
      if (min < DBL_MIN)
	{
	  cout << ", min < DBL_MIN -> set min to log10(max)- 5" << endl;
	  min= log10(max)- 5.;
	}
      else
	min= log10(min);
      scale= maxval/(log10(max)- min);
      for (i= 0; i < arrsize; i++)
	data[i]= (log10(data[i])- min) * scale;
    }
  else  // linscale
    {
      scale= maxval/(max - min);
      for (i= 0; i < arrsize; i++)
	data[i]= (data[i]- min) * scale;
    } 
  cout << ", old (min, max): " << min << ", "<< max << endl;
} // autorange

void calc_alpha(double *data, int rows, int cols, struct ConfigStruct *cs)
{
  int row, col, idxc;
  double myR0, Dn;
  // I do not use cs>R0 I calculate it from  2.18

  myR0= cs->d/((cs->p2* cs->eta/cs->p1)- 1.0);
  Dn= 1.0/(cs->eta * cs->eta) *cs->eta * cs->talbot_n * cs->p1 * cs->p1/ (2* 1240e-9/ cs->photon_energy);

  cout << "calc_alpha: myR0 (m)= "<< myR0 << " cs->R0= " << cs->R0 << endl;
  cout << "calc_alpha: Dn (m)= "<< Dn << " cs->d= " << cs->d << endl;


  if (cs->direction)   // rowdir
    {
      for (row= 0; row < rows; row++)
	for (col= 0; col < cols; col++)
	  {
	    idxc= row* cols+ col;
	    data[idxc]*= row * cs->pixel_size/myR0 + cs->p2/(2* PI* cs->d);
	  }
    } // end rowdir
  else  // coldir
    {
      for (row= 0; row < rows; row++)
	for (col= 0; col < cols; col++)
	  {
	    idxc= row* cols+ col;
	    data[idxc]*= col * cs->pixel_size/myR0 + cs->p2/(2* PI* cs->d);
	  }
    } // end coldir
} // calc_alpha

// calc a 2d forward i.e. fft(-1) - 
// apply fftshift, 
// output amp, wrapped and unwrapped phase
void evalfft2d(Matrix *put_in, struct ConfigStruct *cstp, struct Xgi_out *put_out)
{
  fftw_complex *in, *out;
  fftw_plan    p1;
  int          idxc, idxmax;
  double       amp, pha;
  FLOATING     *data;
  int          rows, cols;

#ifdef DEBUG
  cout << "eval evalfft2d called" << endl;
#endif

  put_in->copy(&put_out->amp);       // copy input to output in field amp,   address= 1
  put_in->copy(&put_out->dphi_wrap); // copy input to output in field phase, address= 2
  rows= put_in->height;
  cols= put_in->width;
  data= put_out->amp.data;           // Array address= 1
  
  idxmax= rows* cols;

  in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * idxmax);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * idxmax);
  p1  = fftw_plan_dft_2d(cols, rows, in, out, FFTW_FORWARD,  FFTW_ESTIMATE);  /* fast init */

  for (idxc= 0; idxc< idxmax; idxc++) // fftw uses c-type arrays - one loop is sufficient
    {
      in[idxc][0]= data[idxc];        // re[idxc];
      in[idxc][1]= 0.0;               // im[idxc];
    }

  fftw_execute(p1);             // forward fft(-1)
  fftshift2d(out, rows, cols);  // center output

  for (idxc= 0; idxc< idxmax; idxc++)
    {
      amp= sqrt(pow(out[idxc][0], 2)+ pow(out[idxc][1], 2)); 
      pha= atan2(out[idxc][1], out[idxc][0]);  // the r phase
      data[idxc]= amp;                         // fill amp
      put_out->dphi_wrap.data[idxc]= pha;      // fill wrap
    }

  switch (cstp->outputs)
    {
    case 1:
      autorange(put_out->amp.data, rows, cols, cstp->logscale); 
      break;
    case 2:
      put_out->dphi_wrap.copy(&put_out->amp);
      autorange(put_out->amp.data, rows, cols, cstp->logscale);
      break;
    case 3:
      put_out->dphi_wrap.copy(&put_out->amp);      // overwrite amp whith phase
      unwrap_phase(put_out->amp.data, rows, cols); // call Herra
      autorange(put_out->amp.data, rows, cols, cstp->logscale);
      break;
    default:
      put_out->dphi_wrap.copy(&put_out->dphi_unwrap);      // cp wrap to unwrap 
      unwrap_phase(put_out->dphi_unwrap.data, rows, cols); // call Herra 
    }

  fftw_destroy_plan(p1);  // clean up
  fftw_free(in); 
  fftw_free(out);

#ifdef DEBUG 
  cout << "return from evalfft2d" << endl;
#endif
} // end evalfft2d

// calc a forward i.e. fft(-1) and backward fft(1)- to test speed
// see evalfft2d 
void evalfft_ifft_2d(Matrix *put_in, struct ConfigStruct *cstp, struct Xgi_out *put_out)
{
  fftw_complex *in, *out;
  fftw_plan     p1, p2;
  int           idxc, idxmax;
  double        amp, pha;
  FLOATING      *data;
  int           rows, cols;

#ifdef DEBUG
  cout << "evalfft_ifft_2d called" << endl;
#endif

  put_in->copy(&put_out->amp);  // copy input to output in field amp, address= 1
  put_in->copy(&put_out->dphi_wrap);
  rows= put_in->height;
  cols= put_in->width;
  data= put_out->amp.data;      // Array address= 1

  idxmax= rows* cols;

  in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * idxmax);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * idxmax);
  p1  = fftw_plan_dft_2d(cols, rows, in, out, FFTW_FORWARD,  FFTW_ESTIMATE); /* fast init */
  p2  = fftw_plan_dft_2d(cols, rows, in, out, FFTW_BACKWARD, FFTW_ESTIMATE); /* fast init */

  // fill_fftw
  for (idxc= 0; idxc< idxmax; idxc++) // fftw uses c-type arrays - one loop is sufficient
    {
      in[idxc][0]= data[idxc];        // re[idxc];
      in[idxc][1]= 0.0;               // im[idxc];
    }
  
  cout << "call fftw_execute" << endl;
  fftw_execute(p1);   // forward fft(-1)

// fill input array
  for (idxc= 0; idxc< idxmax; idxc++)
    {
      in[idxc][0]= out[idxc][0];     // re[idxc];
      in[idxc][1]= out[idxc][1];     // im[idxc];
    }

  cout << "call fftw_execute" << endl;
  fftw_execute(p2);   // backward fft(1) 
  
  for (idxc= 0; idxc< idxmax; idxc++)
    {
      amp= sqrt(pow(out[idxc][0], 2)+ pow(out[idxc][1], 2)); 
      pha= atan2(out[idxc][1], out[idxc][0]);  // the r phase
      data[idxc]= amp;
      put_out->dphi_wrap.data[idxc]= pha;
    }

  switch (cstp->outputs)
    {
    case 1:
      autorange(put_out->amp.data, rows, cols, cstp->logscale); 
      break;
    case 2:
      put_out->dphi_wrap.copy(&put_out->amp);
      autorange(put_out->amp.data, rows, cols, cstp->logscale);
      break;
    case 3:
      put_out->dphi_wrap.copy(&put_out->amp);      // overwrite amp whith phase
      unwrap_phase(put_out->amp.data, rows, cols); // call Herra
      autorange(put_out->amp.data, rows, cols, cstp->logscale);
      break;
    default:
      put_out->dphi_wrap.copy(&put_out->dphi_unwrap);      // cp wrap to unwrap 
      unwrap_phase(put_out->dphi_unwrap.data, rows, cols); // call Herra 
    }

  fftw_destroy_plan(p1);
  fftw_destroy_plan(p2);
  fftw_free(in); 
  fftw_free(out);

#ifdef DEBUG
    cout << "return from evalfft_ifft_2d" << endl;
#endif
} // end evalfft_ifft_2d

// main test routine by UF to add functions
void evaluf(Matrix *put_in, struct ConfigStruct *cstp, struct Xgi_out *put_out)
{
  int row, col, val4overlay, rows, cols;
  double m, y0, r;
  FLOATING *data;
    
#ifdef UINT16_OUT
  val4overlay= 0xFFFF;
#else
  val4overlay= 0xFF;
#endif

  put_in->copy(&put_out->amp);  // copy input to output in field amp, address= 1
  put_in->copy(&put_out->dphi_wrap);
  rows= put_in->height;
  cols= put_in->width;
  data= put_out->amp.data;        // Array address= 1
  
  switch (cstp->minor)  // minor calculation mode
    {
    case 0: // same as 5
      cout << "do just scaling" << endl;
      autorange(data, rows, cols, cstp->logscale);  
      break;
    case 1:
      hanning(data, rows, cols, cstp->direction);   // hanning filter
      autorange(data, rows, cols, cstp->logscale);  // cstp->logscale
      break;
    case 2: // fft(-1)
      hanning(data, rows, cols, cstp->direction);   // hanning filter
      eval1d(data, rows, cols, cstp, put_out);      // fft stuff
      autorange(data, rows, cols, cstp->logscale);  // cstp->logscale
      overlay(data, rows, cols, cstp->center, cstp->width, (double)val4overlay, cstp->direction);  // add a overlay
      break;
    case 3: // filter
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      eval1d(data, rows, cols, cstp, put_out); // fft stuff
      autorange(data, rows, cols, cstp->logscale);
      break;
    case 4: // shift
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      eval1d(data, rows, cols, cstp, put_out); // fft stuff
      autorange(data, rows, cols, cstp->logscale);
      break;
    case 5:  // FFT(1)
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      eval1d(data, rows, cols, cstp, put_out);              // fft stuff 
      autorange(data, rows, cols, cstp->logscale);  
      break;
    case 6:   // no scaling
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      eval1d(data, rows, cols, cstp, put_out);              // fft stuff
      break;
    default:
      cout << "error: undefined minor mode, minor= " << cstp->minor << endl;
    }

  if (cstp->direction == COLDIR)
    {
      put_out->hp.resize(1, cols);
      put_out->laxis.resize(1, cols);
      for (col= 0; col< cols; col++)  // extract here just cols
	{
	  put_out->laxis.data[col]= col;
	  put_out->hp.data[col]= data[col + rows/2 * cols]; // center
	}
      
      r= linreg(cols, put_out->laxis.data, put_out->hp.data, &m, &y0);
      cout << "fit results: (corr, m, y0): " << r << ", " << m << ", " << y0 << endl;
      
      for (col= 0; col< cols; col++)       // subtract line
	put_out->hp.data[col]-= (m* put_out->laxis.data[col]+ y0);  //
    }
  else
    { // rowdir
      put_out->hp.resize(1, rows);
      put_out->laxis.resize(1, rows);
      for (row= 0; row< rows; row++)  // 
	{
	  put_out->laxis.data[row]= row;
	  put_out->hp.data[row]= data[cols/2 + row * cols]; // center
	}
      r= linreg(cols, put_out->laxis.data, put_out->hp.data, &m, &y0);
      cout << "fit results: (corr, m, y0): " << r << ", " << m << ", " << y0 << endl;
      
      for (row= 0; row< rows; row++)       // subtract line
	put_out->hp.data[row]-= (m* put_out->laxis.data[row]+ y0);  //
    } // end rowdir
  
  put_out->roc= (fabs(m) > DBL_MIN) 
      ? 1/m : 0; // formula just a guess

  switch (cstp->outputs)
    {
    case 2:
      put_out->dphi_wrap.copy(&put_out->amp);
      break;
    case 3:
      put_out->dphi_unwrap.copy(&put_out->amp);      // overwrite amp whith phase
      break;
    }


  cout << "return from evaluf" << endl;
} // end evaluf()

// main test routine by UF to add functions with zero padding
void evaluf_zp(Matrix *put_in, struct ConfigStruct *cstp, struct Xgi_out *put_out)
{
  int row, col, val4overlay, rows, cols;
  double m, y0, r;
  FLOATING *data;
  
#ifdef UINT16_OUT
  val4overlay= 0xFFFF;
#else
  val4overlay= 0xFF;
#endif

  put_in->copy(&put_out->amp);  // copy input to output in field amp, address= 1
  put_in->copy(&put_out->dphi_wrap);
  rows= put_in->height;
  cols= put_in->width;
  data= put_out->amp.data; // Array address= 1

  //  direction= COLDIR;     // evaluate horizontal cuts

  switch (cstp->minor)  // minor calculation mode
    {
    case 0: // same as 5
      cout << "do just scaling" << endl;
      autorange(data, rows, cols, cstp->logscale);  
      break;
    case 1:
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      cout << "just hanning not evailable with zero padding" << endl;
      autorange(data, rows, cols, cstp->logscale);  // logscale
      break;
    case 2: // fft(-1)
      hanning(data, rows, cols, cstp->direction);   // hanning filter
      eval1d_zp(data, rows, cols, cstp, put_out);   // fft stuff
      autorange(data, rows, cols, cstp->logscale);  // logscale
      overlay(data, rows, cols, cstp->center, cstp->width, (double)val4overlay, cstp->direction);  // add a overlay
      break;
    case 3: // filter
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      eval1d_zp(data, rows, cols, cstp, put_out);  // fft stuff
      autorange(data, rows, cols, cstp->logscale);
      break;
    case 4: // shift
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      eval1d_zp(data, rows, cols, cstp, put_out);  // fft stuff
      autorange(data, rows, cols, cstp->logscale);
      break;
    case 5:  // FFT(1)
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      eval1d_zp(data, rows, cols, cstp, put_out);  // fft stuff 
      autorange(data, rows, cols, cstp->logscale);  
      break;
    case 6:   // no scaling
      hanning(data, rows, cols, cstp->direction);  // hanning filter
      eval1d_zp(data, rows, cols, cstp, put_out);  // fft stuff
      break;
    default:
      cout << "error: undefined minor mode, minor= " << cstp->minor << endl;
    }

  if (cstp->direction == COLDIR)
    {
      put_out->hp.resize(1, cols);
      put_out->laxis.resize(1, cols);
      for (col= 0; col< cols; col++)  // extract here just cols
	{
	  put_out->laxis.data[col]= col;
	  put_out->hp.data[col]= data[col + rows/2 * cols]; // center
	}
      
      r= linreg(cols, put_out->laxis.data, put_out->hp.data, &m, &y0);
      cout << "fit results: (corr, m, y0): " << r << ", " << m << ", " << y0 << endl;
      
      for (col= 0; col< cols; col++)       // subtract line
	put_out->hp.data[col]-= (m* put_out->laxis.data[col]+ y0);  //
    }
  else
    { // rowdir
      put_out->hp.resize(1, rows);
      put_out->laxis.resize(1, rows);
      for (row= 0; row< rows; row++)  // 
	{
	  put_out->laxis.data[row]= row;
	  put_out->hp.data[row]= data[cols/2 + row * cols]; // center
	}
      
      r= linreg(cols, put_out->laxis.data, put_out->hp.data, &m, &y0);
      cout << "fit results: (corr, m, y0): " << r << ", " << m << ", " << y0 << endl;
      
      for (row= 0; row< rows; row++)       // subtract line
	put_out->hp.data[row]-= (m* put_out->laxis.data[row]+ y0);  //xprof[row]-= (m* yprof[row]+ y0);  //
    } // end rowdir
  
  put_out->roc= (fabs(m) > DBL_MIN) 
      ? 1/m : 0; // formula just a guess
  
  switch (cstp->outputs)
    {
    case 2:
      put_out->dphi_wrap.copy(&put_out->amp);
      break;
    case 3:
      put_out->dphi_unwrap.copy(&put_out->amp);      // overwrite amp whith phase
      break;
    }
  cout << "return from evaluf_zp" << endl;
} // end evaluf_zp()

// makes a 1d fft, default is cols plus other treatment
void eval1d(double *data, int rows, int cols, struct ConfigStruct *cs, struct Xgi_out *put_out)
{
  fftw_complex *in, *out;
  fftw_plan     p1, p2;
  size_t        arrsize;
  int           row, col, idxc;
  
#ifdef DEBUG 
  cout << "eval1d called" << endl;
#endif

  if (cs->direction)   // rowdir
    {
      arrsize= sizeof(fftw_complex) * rows;
      in = (fftw_complex*) fftw_malloc(arrsize);
      out= (fftw_complex*) fftw_malloc(arrsize);
      p1 = fftw_plan_dft_1d(rows, in, out, FFTW_FORWARD,  FFTW_ESTIMATE);
      p2 = fftw_plan_dft_1d(rows, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);

      for (col= 0; col < cols; col++)
	{
	  for (row= 0; row < rows; row++)
	    {
	      idxc= row* cols+ col;
	      in[row][0]= data[idxc];     // re[idxc];
	      in[row][1]= 0.0;            // im[idxc];
	    }
	  fftw_execute(p1);               // forward fft(-1)
	  fftshift1d(out, rows);

	  switch (cs->minor) // go ahead with filter etc 
	    {
	    case 2:
	      break;
	    case 3:
	      filter1d(out, rows, 0, cs->center, cs->width); // do not move center
	      break;
	    case 4:
	      filter1d(out, rows, 1, cs->center, cs->width); // do move center
	      break;
	    case 5:
	    case 6:  
	      filter1d(out, rows, 1, cs->center, cs->width);
	      memcpy(in, out, arrsize);
	      fftw_execute(p2);
	      break;
	    case 7:
              break;  
	    default:
	      cout << "error - unexpected minor - exit(-1)" << endl;
	      exit(-1);
	    }
	  
	  for (row= 0; row < rows; row++)
	    {
	      idxc= col+ row* cols;
	      put_out->dphi_wrap.data[idxc]= atan2(out[row][1], out[row][0]);
	      data[idxc]= sqrt(pow(out[row][0], 2)+ pow(out[row][1], 2));
	    }
	} // colloop

      fftw_destroy_plan(p1);
      fftw_destroy_plan(p2);
      fftw_free(in); 
      fftw_free(out);
    } // end rows
  else // cols
    {
      arrsize= sizeof(fftw_complex) * cols;
      in = (fftw_complex*) fftw_malloc(arrsize);
      out= (fftw_complex*) fftw_malloc(arrsize);
      p1 = fftw_plan_dft_1d(cols, in, out, FFTW_FORWARD,  FFTW_ESTIMATE);
      p2 = fftw_plan_dft_1d(cols, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);

      for (row= 0; row < rows; row++)
	{
	  for (col= 0; col < cols; col++)
	    {
	      idxc= row* cols+ col;
	      in[col][0]= data[idxc];     // re[idxc];
	      in[col][1]= 0.0;            // im[idxc];
	    }
	  fftw_execute(p1);               // forward fft(-1)
	  fftshift1d(out, cols);

	  switch (cs->minor) // go ahead with filter etc 
	    {
	    case 2:
	      break;
	    case 3:
	      filter1d(out, cols, 0, cs->center, cs->width); // do not move center
	      break;
	    case 4:
	      filter1d(out, cols, 1, cs->center, cs->width); // do move center
	      break;
	    case 5:
	    case 6:  
	      filter1d(out, cols, 1, cs->center, cs->width);
	      memcpy(in, out, arrsize);
	      fftw_execute(p2);
	      break;
	    case 7:
              break;  
	    default:
	      cout << "error - unexpected minor - exit(-1)" << endl;
	      exit(-1);
	    }
	  
	  for (col= 0; col < cols; col++)
	    {
	      idxc= col+ row* cols;
	      put_out->dphi_wrap.data[idxc]= atan2(out[col][1], out[col][0]);
	      data[idxc]= sqrt(pow(out[col][0], 2)+ pow(out[col][1], 2));
	    }
	} // rowloop
      fftw_destroy_plan(p1);
      fftw_destroy_plan(p2);
      fftw_free(in); 
      fftw_free(out);
    } // else cols

  switch (cs->outputs)
    {
    case 2:
      put_out->dphi_wrap.copy(&put_out->amp);
      break;
    case 3:
      put_out->dphi_wrap.copy(&put_out->amp);
      unwrap_phase(put_out->amp.data, rows, cols); // call Herra
      break;
    case 4:
      put_out->dphi_wrap.copy(&put_out->amp);
      unwrap_phase(put_out->amp.data, rows, cols); // call Herra
      calc_alpha(put_out->amp.data, rows, cols, cs);
      break;
    default:
	put_out->dphi_wrap.copy(&put_out->dphi_unwrap);
	unwrap_phase(put_out->dphi_unwrap.data, rows, cols);
    }

} // end eval1d


// makes a 1d fft, default is cols plus other treatment incl zero padding 
void eval1d_zp(double *data, int rows, int cols, struct ConfigStruct *cs, struct Xgi_out *put_out)
{
  fftw_complex *in, *out;
  fftw_plan     p1, p2;
  size_t        arrsize;
  int           row, col, i, i0, len0, imax, idxc, fftlen[8]= {64, 128, 256, 512, 1024, 2048, 4096, 8192};
  
  
#ifdef DEBUG 
  cout << "eval1d_zp called" << endl;
#endif  

  
  
  len0= (cs->direction) ? rows : cols;
  i= 0;
  while ((i < 8) && (fftlen[i] < len0)) i++;
  imax= (i < 6) ? fftlen[i+ cs->zeropadding] : fftlen[7];
  i0= (imax- len0)/ 2;
  cout << "fftpoints (old, new, i0, ZEROPADDING): " << len0 << ", " << imax << ", " << i0 << ", " << cs->zeropadding << endl;

  arrsize= sizeof(fftw_complex) * imax;
  in = (fftw_complex*) fftw_malloc(arrsize);
  out= (fftw_complex*) fftw_malloc(arrsize);
  p1 = fftw_plan_dft_1d(imax, in, out, FFTW_FORWARD,  FFTW_ESTIMATE);
  p2 = fftw_plan_dft_1d(imax, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  
  if (cs->direction)
    {
      for (col= 0; col< cols; col++)
	{
	  bzero((void *)in, arrsize);           // fill all zero
	  for (i= 0; i< len0; i++)
	    in[i+ i0][0]= data[col + i* cols];  // this is the difference
	    
	  fftw_execute(p1);                     // forward fft(-1)
	  fftshift1d(out, imax);
	  switch (cs->minor) // go ahead with filter etc 
	    {
	    case 2:
	      break;
	    case 3:
	      filter1d(out, imax, 0, (i0+ cs->center), cs->width); // do not move center
	      break;
	    case 4:
	      filter1d(out, imax, 1, (i0+ cs->center), cs->width); // do move center
	      break;
	    case 5:
	    case 6:  
	      filter1d(out, imax, 1, (i0+ cs->center), cs->width);
	      memcpy(in, out, arrsize);
	      fftw_execute(p2);
	      break;
	    case 7:
              break;  
	    default:
	      cout << "error - unexpected minor - exit(-1)" << endl;
	      exit(-1);
	    }
	  
	    for (i= 0; i< len0; i++)
	      {
		idxc= col+ i* cols;
		put_out->dphi_wrap.data[idxc]= atan2(out[i+ i0][1], out[i+ i0][0]);
		data[idxc]= sqrt(pow(out[i+ i0][0], 2)+ pow(out[i+ i0][1], 2));
	      }
	} // colloop
    } // end rows
  else // cols
    {
      for (row= 0; row< rows; row++)
	{
	  bzero((void *)in, arrsize);           // fill all zero
	  for (i= 0; i< len0; i++)
	    in[i+ i0][0]= data[i + row* cols];  // this is the difference
	    
	  fftw_execute(p1);                     // forward fft(-1)
	  fftshift1d(out, imax);
	  switch (cs->minor) // go ahead with filter etc 
	    {
	    case 2:
	      break;
	    case 3:
	      filter1d(out, imax, 0, (i0+ cs->center), cs->width); // do not move center
	      break;
	    case 4:
	      filter1d(out, imax, 1, (i0 + cs->center), cs->width); // do move center
	      break;
	    case 5:
	    case 6:  
	      filter1d(out, imax, 1, (i0+ cs->center), cs->width);
	      memcpy(in, out, arrsize);
	      fftw_execute(p2);
	      break;
	    case 7:
              break;  
	    default:
	      cout << "error - unexpected minor - exit(-1)" << endl;
	      exit(-1);
	    }
	  
	    for (i= 0; i< len0; i++)
	      {
		idxc= i+ row* cols;
		put_out->dphi_wrap.data[idxc]= atan2(out[i+ i0][1], out[i+ i0][0]);
		data[idxc]= sqrt(pow(out[i+ i0][0], 2)+ pow(out[i+ i0][1], 2));
	      }
	} // rowloop
    } // else cols
  
  fftw_destroy_plan(p1);
  fftw_destroy_plan(p2);
  fftw_free(in); 
  fftw_free(out);
  switch (cs->outputs)
    {
    case 2:
      put_out->dphi_wrap.copy(&put_out->amp);
      break;
    case 3:
      put_out->dphi_wrap.copy(&put_out->amp);
      unwrap_phase(put_out->amp.data, rows, cols); // call Herra
      break;
    default:
	put_out->dphi_wrap.copy(&put_out->dphi_unwrap);
	unwrap_phase(put_out->dphi_unwrap.data, rows, cols);
    }
} // end eval1d_zp

/* shift frequencies to center                                   */
/* to be checked whether it works for even and odd - yes it does */
void fftshift2d(fftw_complex *arr0, int rows, int cols)
{
  fftw_complex *arr1;
  size_t arrsize;
  int row, col, nrow, ncol, rows2, cols2, idxin, idxout;
  
  arrsize= sizeof(fftw_complex) * rows * cols;
  arr1= (fftw_complex*) fftw_malloc(arrsize);
  memcpy(arr1, arr0, arrsize);                            /* save initial array */

  rows2= rows/ 2;
  cols2= cols/ 2;

  //  printf("fftshift: rows=%d, row2= %d\n", rows, rows2);

  for (row= 0; row< rows; row++)
    {
      //nrow= (row <= rows2) ? (rows2 + row) : (row- rows2- 1);
      for (col= 0; col< cols; col++)
	{
	  ncol= (col+ cols2) % cols;
	  nrow= (row+ rows2) % rows;
	  //ncol= (col <= cols2) ? (cols2 + col) : (col- cols2- 1);
	  
	  idxin = col+  row*  cols;
	  idxout= ncol+ nrow* cols;
	  
	  arr0[idxout][0]= arr1[idxin][0];
	  arr0[idxout][1]= arr1[idxin][1];
	}
    }
  fftw_free(arr1);
} /* end fftshift */

/* shift frequencies to center                                   */
/* to be checked whether it works for even and odd - yes it does */
// just col version
void fftshift1d(fftw_complex *arr0, int cols)
{
  fftw_complex *arr1;
  size_t arrsize;
  int col, ncol, cols2;
  
  arrsize= sizeof(fftw_complex) * cols;
  arr1= (fftw_complex*) fftw_malloc(arrsize);
  memcpy(arr1, arr0, arrsize);                            /* save initial array */
  cols2= cols/ 2;
  for (col= 0; col< cols; col++)
    {
      ncol= (col+ cols2) % cols;
      arr0[ncol][0]= arr1[col][0];
      arr0[ncol][1]= arr1[col][1];
    }
   fftw_free(arr1);
} /* end fftshift1d */

// cut out one range and center it center should be a pixel number
// version just for horizontal i.e. rowdir=0
void filter1d(fftw_complex *arr0, int cols, int movecenter, int center, int width)
{
  fftw_complex *arr1;
  size_t arrsize;
  int ncol, col, i, col0, ncol0;

#ifdef DEBUG3
  cout << "filter1d called, movecenter= " << movecenter << ", cols= " << cols << endl;
#endif

  col0= center - width/2;                       // first column in
  ncol0= (movecenter) ? cols/2- width/2 : col0; // first column out
  arrsize= sizeof(fftw_complex) * cols;
  arr1= (fftw_complex*) fftw_malloc(arrsize);
  memcpy(arr1, arr0, arrsize);
  bzero((void*)arr0, arrsize);                  // zero arr0
  //std::fill()
  for (i= 0; i< width; i++)
    {
      col = col0  + i;
      ncol= ncol0 + i;
      arr0[ncol][0]= arr1[col][0];
      arr0[ncol][1]= arr1[col][1];
    }
  fftw_free(arr1);
} // filter1d

// 1d hanning taper in rows of cols (cols is default)
void hanning(double *data, int rows, int cols, int rowdir)
{
  int row, col;
  double wind, t;

#ifdef DEBUG
  cout << "hanning called" << endl;
#endif

  /* the idl code */
  // t= 2.0* np.arange(L)/L - 1.0
  // wind= 1.0 + np.cos(np.pi*t)
  if (rowdir)
    {
      for (row= 0; row< rows; row++)
	{
	  t= 2.0* row/(double)rows - 1.0;
	  wind= 1.0+ cos(PI* t);
	  for (col= 0; col< cols; col++)
	    data[col+ row* cols]*= wind;
	} 
    }
  else  // coldir
    {
      for (col= 0; col< cols; col++)
	{
	  t= 2.0* col/(double)cols - 1.0;
	  wind= 1.0+ cos(PI* t);
	  for (row= 0; row< rows; row++)
	    data[col+ row* cols]*= wind;
	}
    }
} // end hanning

// 1d hanning taper of a vector in place
void hanning1d(double *data, int len)
{
  int i;
  double wind, t;

#ifdef DEBUG
  cout << "hanning1d called" << endl;
#endif

  /* the idl code */
  // t= 2.0* np.arange(L)/L - 1.0
  // wind= 1.0 + np.cos(np.pi*t)
  for (i= 0; i< len; i++)
    {
      t= 2.0* i/(double)len - 1.0;
      wind= 1.0+ cos(PI* t);
      data[i]*= wind;
    } 
} // end hanning1d



// checks whether a file exists
bool is_file_exist(const char *fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
} // is_file_exist


// linear regression, returns correlation coefficient
double linreg(int n, double *x, double *y, double *m, double *y0)
{
  double r, sumx= 0, sumx2= 0, sumy= 0, sumy2= 0, sumxy= 0, denom;
  int i;
  
#ifdef DEBUG
  cout << "linreg called" << endl;
#endif

  for (i= 0; i< n; i++)
    {
      sumx  += x[i];
      sumx2 += pow(x[i], 2.0);
      sumxy += x[i] * y[i];
      sumy  += y[i];
      sumy2 += pow(y[i], 2.0);
    }

  denom= n* sumx2- pow(sumx, 2);
  if (fabs(denom) < DBL_MIN) // singular matrix. can't solve the problem.
    {           
      *m = 0;
      *y0= 0;
      r  = 0;
    }
  else
    {
      *m= (n* sumxy- sumx* sumy)/ denom;
      *y0= (sumy* sumx2-  sumx* sumxy)/ denom;
      r= (sumxy- sumx* sumy/ n)/          /* compute correlation coeff     */
	sqrt((sumx2- pow(sumx, 2)/n) *
	     (sumy2- pow(sumy, 2)/n));
    }
  return r;
} // linreg


// simple overlay for testing 
// print UF at lower left corner of Image in output field amp
void myoverlay(Matrix *put_in, struct ConfigStruct *cstp, struct Xgi_out *put_out)
{
  int row, col, row0= 80, col0= 10, row1, col1, row2, col2, height= 20, width= 10, val= 255;
  FLOATING *data;
  int rows, cols;
  
#define UINT16_OUT
#ifdef UINT16_OUT
  val= 0xFFFF;  // 65535
#endif 

  rows= put_in->height;
  cols= put_in->width;
  put_in->copy(&put_out->amp);       // copy input to output in field amp,   address= 1
  data= put_out->amp.data;           // Array address= 1
  
  cout << "put myoverlay  @ (row, col, val) " << row0 << ", " << col0 << ", " << val << endl;

  row0= rows- row0; // plot at bottom

  row1= row0+ height;
  row2= row0+ height/ 2;
  col1= col0+ width;
  col2= col0+ 2* width;
  
  for (row=row0; row< row1; row++)  // vertical lines
    { 
      data[col0+ row* cols]= val;
      data[col1+ row* cols]= val;
      data[col2+ row* cols]= val;
      data[col0+ row* cols+ 1]= val;
      data[col1+ row* cols+ 1]= val;
      data[col2+ row* cols+ 1]= val;
    }

  for (col= col0; col< col1; col++) // horizontal lines
    { 
      data[col+ row1* cols]= val;
      data[col+ 2* width+ row0* cols]= val;
      data[col+ 2* width+ row2* cols]= val;
      data[col+ (row1+ 1)* cols]= val;
      data[col+ 2* width+ (row0+ 1)* cols]= val;
      data[col+ 2* width+ (row2+ 1)* cols]= val;
    }

  put_out->amp.copy(&put_out->dphi_wrap);     // export all arrays      
  put_out->amp.copy(&put_out->dphi_unwrap); 
} // end myoverlay

// overlay lines at the filter values in the data array
void overlay(double *data, int rows, int cols, int center, int width, double val, int rowdir)
{
  int row, col1, col2;
  int col, row1, row2;

#ifdef DEBUG
  cout << "overlay called with val: " << val << endl;
#endif

  if (rowdir)
    {
      row1= center- width/ 2 - 1;
      row2= center+ width/ 2 + 1;
      if ((row1 < 2) || (row2 > (rows- 3)))
	{
	  cout << "error: center and/or width out of range- return" << endl;
	  return;
	}

      for (col= 0; col < cols; col++)
	{
	  data[col + row1* cols]= val;
	  data[col + row2* cols]= val;
	  data[col + (row1- 1)* cols]= val;
	  data[col + (row2+ 1)* cols]= val;
	  data[col + (row1- 2)* cols]= val;
	  data[col + (row2+ 2)* cols]= val;
	}
    }
  else
    {
      col1= center- width/ 2 - 1;
      col2= center+ width/ 2 + 1;
      if ((col1 < 2) || (col2 > (cols- 3)))
	{
	  cout << "error: center and/or width out of range- return" << endl;
	  return;
	}
      for (row= 0; row < rows; row++)
	{
	  data[col1 + row* cols]= val;
	  data[col2 + row* cols]= val;
	  data[col1- 1 + row* cols]= val;
	  data[col2+ 1 + row* cols]= val;
	  data[col1- 2 + row* cols]= val;
	  data[col2+ 2 + row* cols]= val;
	}
    }
} // overlay

// read config from json? file filename hardcoded in CONFIGFILENAME
void ReadConfig(struct ConfigStruct *cs)
{
  int lines= 0, ok= 0;
  std::string line, key, value;
  //std::string::size_type sz;     // alias of size_t
  char *pend;

  cout << "****************************************************"  << endl;
  cout << "UF ReadConfig called, filename: " << CONFIGFILENAME    << endl;
  cout << "****************************************************"  << endl;

  // define defaults in case the configfile is not found or values not defined
  cs->zeropadding= 19990101;
  cs->zeropadding= 1;
  cs->mytestdouble= 2.1;
  cs->outputs= 0;
  cs->logscale= 0;
  cs->averages= 1;

  if (!is_file_exist(CONFIGFILENAME))
    {
      cout << "no configfile >>" << CONFIGFILENAME << "<< return" << endl;
      return;
      // exit(-1);
    }
  
  std::ifstream is_file(CONFIGFILENAME);

  while( std::getline(is_file, line) )
    {
      lines++;
      std::istringstream is_line(line);
      if( std::getline(is_line, key, '=') )
	{
	  if( std::getline(is_line, value) ) 
	    {
	      if ( !strcmp("version",       key.c_str())) {cs->version      = strtol(value.c_str(), &pend, 10); ok++; }
	      if ( !strcmp("zeropadding",   key.c_str())) {cs->zeropadding  = strtol(value.c_str(), &pend, 10); ok++; }
	      if ( !strcmp("mytestdouble",  key.c_str())) {cs->mytestdouble = strtod(value.c_str(), &pend); ok++; }
              if ( !strcmp("d",             key.c_str())) {cs->d            = strtod(value.c_str(), &pend); ok++; }
              if ( !strcmp("eta",           key.c_str())) {cs->eta          = strtol(value.c_str(), &pend, 10); ok++; }
              if ( !strcmp("p1",            key.c_str())) {cs->p1           = strtod(value.c_str(), &pend); ok++; }
              if ( !strcmp("p2",            key.c_str())) {cs->p2           = strtod(value.c_str(), &pend); ok++; }
              if ( !strcmp("photon_energy", key.c_str())) {cs->photon_energy= strtod(value.c_str(), &pend); ok++; }
              if ( !strcmp("pixel_size",    key.c_str())) {cs->pixel_size   = strtod(value.c_str(), &pend); ok++; }
              if ( !strcmp("R0",            key.c_str())) {cs->R0           = strtod(value.c_str(), &pend); ok++; }
              if ( !strcmp("talbot_n",      key.c_str())) {cs->talbot_n     = strtol(value.c_str(), &pend, 10); ok++; }
	      
	    }
	}
      cout << "parse line " << lines << " >>" << line << "<<, valid: " << ok << endl; // for debugging
    } // while line

  cout << "version: " << cs->version << endl;
  cout << "photon_energy: " << cs->photon_energy << endl;
  cout << "end ReadConfig lines: " << lines << " valid: " << ok << endl;
  cout << "****************************************************"  << endl;
  
} // ReadConfig



// end algorithms.cpp
